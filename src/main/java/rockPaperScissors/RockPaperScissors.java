package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 0; // I changed this from 1 to 0 because in my head the counter should count only when the round actually starts, aka. when run() runs
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while (true) {
            // Intros
            roundCounter++;
            System.out.println("Let's play round " + roundCounter);

            // Selecting between rock, paper or scissors
            String humanChoice = userChoice();
            String computerChoice = randomChoice();
            String choiceString = "Human chose "+ humanChoice + ", computer chose " + computerChoice + ".";

            // Determine winner and add score
            if (isWinner(humanChoice, computerChoice)) {
                System.out.println(choiceString + " Human wins.");
                humanScore++;
            }
            else if (isWinner(computerChoice, humanChoice)) {
                System.out.println(choiceString + " Computer wins.");
                computerScore++;
            }
            else {
                System.out.println(choiceString + " It's a tie");
            }
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            // See if user wants to play again. Computer has no choice.
            char continueAnswer = continuePlaying().charAt(0); // charAt(index) converts first letter (index 0) of String to char
            if (continueAnswer == 'n') {
                break;
            }
        }
        System.out.println("Bye bye :)");
    
    }

    public String randomChoice() {
        /*
        Chooses random out of rock paper scissors
        :return: random choice
        */
        // Following two lines of code is from Martin Vatshelle (Martin.Vatshelle@uib.no)
        Double randNum = Math.random()*3; //number between 0 and 2.9999..
        int choice = randNum.intValue(); // int with value 0,1 or 2
        return rpsChoices.get(choice);
    }

    public boolean isWinner(String choice1, String choice2) {
        /*
        Check if choice1 is wins over choice2
        :param choice1:
        :param choice2:
        :return: true if choice1 beats choice2, false if not
        */
        if (choice1.equals("paper")) { // Apparently (string1 == string2) doesn't work in java
            return choice2.equals("rock");
        }
        else if (choice1.equals("scissors")) {
            return choice2.equals("paper");
        }
        else {
            return choice2.equals("scissors");
        }
    }

    public String userChoice() {
        /*
        Prompt the user with what choice of rock paper scissors they choose
        :return: "rock", "paper" or "scissors"
        */
        while (true) {
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            String[] choices = rpsChoices.toArray(new String[0]); // converts from list to array, because I don't yet fully understand the difference
            if (validateInput(humanChoice, choices)) {
                return humanChoice;
            }
            else {
                System.out.println("I don't understand " + humanChoice + ". Try again");
            }
        }
    }

    public String continuePlaying() {
        /*
        Prompt the user if they want to continue playing.
        :return: "y" or "n"
        */
        while (true) {
            String continueAnswer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase(); /* equal to .lower() in python */
            String[] validInputs = {"y", "n"};
            if (validateInput(continueAnswer, validInputs)) {
                return continueAnswer;
            }
            else {
                System.out.println("I don't understand " + continueAnswer + ". Try again");
            }
        }
    }

    public boolean validateInput(String input, String[] validInput) {
        /*
        Checks if the given input is either rock, paper or scissors.
        :param input: user input string
        :param valid_input: list of valid input strings
        :return: true if valid input, false if not
        */
        input = input.toLowerCase();
        return Arrays.asList(validInput).contains(input); // in python this would be "return input in validInput"
    }



    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
}
